const path = require("path");
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: {
    index: "./src/index.js",
    // html: "./src/index.html",
  },
  output: {
    // filename: "[name].js",
    path: path.join(__dirname, ''),
  },
  mode: "production",
  target: "node",
  externals: [nodeExternals()],
  node: {
    __dirname: false,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/env"],
            plugins: [
              "@babel/proposal-async-generator-functions",
              "@babel/transform-runtime",
              "@babel/proposal-class-properties",
              "transform-class-properties"
            ],
          },
        },
      },
    ],
  },
};

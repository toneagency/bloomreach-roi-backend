const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const PDF = require("pdf-lib");
const fs = require("fs");
const fetch = require("isomorphic-unfetch");
const { PDFDocument, rgb } = require("pdf-lib");
const randomString = require("random-base64-string");
var AWS = require("aws-sdk");
var path = require("path");
var PDFLocations = require("./pdf.json");
var fontkit = require("@pdf-lib/fontkit");



const rawdata = fs.readFileSync('./config.json');
const Config = JSON.parse(rawdata);


const app = express();
const port = 8030;

const corsOptions = {
  origin: "*",
};

app.use(cors(corsOptions));
app.options("*", cors(corsOptions));

app.use(bodyParser.json()); // to support JSON-encoded bodies

app.post("/", function (req, res, next) {
  var PDF = new modifyPDF(req.body);

  PDF.generatePDF()
    .then((fileName) => {
      let pdfURL = uploadPDF(fileName);
      res.send({ url: pdfURL });
    })
    .catch((err) => console.error(err));
});

app.get("/", (req, res) => {
  // res.render(__dirname + "/html.html");
  res.sendFile(__dirname + "/index.html");
});

app.listen(port);

class modifyPDF {
  constructor(inputData) {
    // Gets all the values from the input Data
    [this.PDFMM, this.PDFMS, this.totalMade, this.totalSaved] = inputData;

    // PDF pages
    this.acceptedPages = [0, 9]; // Cover is always shown
    this.declinedPages = [1, 2, 3, 4, 5, 6, 7, 8]; // Hide all pages, and unhide when theres an input

    // Generates a random URL string for the PDF Name
    this.URL = `${randomString(4)}-${Date.now()}-${randomString(16)}.pdf`;

    this.PDFMM.forEach((stat) => {
      if (stat.page == "siteSearch") this.sortPage(1);
      else if (stat.page == "siteMerch") this.sortPage(2);
      else if (stat.page == "personalization") this.sortPage(3);
      else if (stat.page == "content") this.sortPage(4);
      else if (stat.page == "organicSearch") this.sortPage(5);
    });

    this.PDFMS.forEach((stat) => {
      if (stat.page == "CMS") this.sortPage(6);
      else if (stat.page == "replatform") this.sortPage(7);
      else if (stat.page == "no-replatform") this.sortPage(8);
    });
  }

  // Adds and removes pages from the arrays with determine which pages generate in the PDF
  sortPage = (pageNumber) => {
    let pos = this.declinedPages.indexOf(pageNumber);
    this.declinedPages.splice(pos, 1);
    this.acceptedPages.push(pageNumber);
  };

  convertToCurrency = (int) => {
    return int.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    });
  };

  generatePDF = async () => {
    var templateURL = Config.pdfURL;
    var templateBytes = await fetch(templateURL).then((res) =>
      res.arrayBuffer()
    );

    var pdfDoc = await PDFDocument.load(templateBytes);

    pdfDoc.registerFontkit(fontkit);

    const url =
      "https://use.typekit.net/af/4eabcf/00000000000000003b9b12fd/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3";

    const fontBytes = await fetch(url).then((res) => res.arrayBuffer());

    const font = await pdfDoc.embedFont(fontBytes);

    const pages = pdfDoc.getPages();

    PDFLocations.PDFMoneyMade.forEach((item, i) => {
      this.PDFMM.forEach((page, ii) => {
        if (item.pageName == page.page) {
          pages[item.pageID].drawText(`${this.convertToCurrency(page.value)}`, {
            x: item.x,
            y: item.y,
            font: font,
            size: 16,
            color: rgb(0, 0.12, 0.192),
          });
        }
      });
    });

    PDFLocations.PDFCostSaved.forEach((item, i) => {
      this.PDFMS.forEach((page, ii) => {
        if (item.pageName == page.page) {
          pages[item.pageID].drawText(`${this.convertToCurrency(page.value)}`, {
            x: item.x,
            y: item.y,
            font: font,
            size: 16,
            color: rgb(0, 0.12, 0.192),
          });
        }
      });
    });

    // Make
    pages[0].drawText(`${this.convertToCurrency(this.totalMade)}`, {
      x: 64,
      y: 226,
      font: font,
      size: 22,
      color: rgb(0, 0.12, 0.192),
    });

    // Save
    pages[0].drawText(`${this.convertToCurrency(this.totalSaved)}`, {
      x: 248,
      y: 226,
      font: font,
      size: 22,
      color: rgb(0, 0.12, 0.192),
    });

    // 1 Year ROI
    pages[0].drawText(
      `${this.convertToCurrency(this.totalMade + this.totalSaved)}`,
      {
        x: 170,
        y: 155,
        font: font,
        size: 22,
        color: rgb(0, 0.12, 0.192),
      }
    );

    // 3 Year ROI
    pages[0].drawText(
      `${this.convertToCurrency((this.totalMade + this.totalSaved) * 3)}`,
      {
        x: 170,
        y: 89,
        font: font,
        size: 22,
        color: rgb(0, 0.12, 0.192),
      }
    );

    // This needs to be done last, so all text is written on the right pages
    for (let i = 10; i > 0; i--) {
      this.declinedPages.indexOf(i) > -1 ? pdfDoc.removePage(i) : null;
    }

    const pdfBytes = await pdfDoc.save();

    fs.writeFileSync(`./generated/${this.URL}`, pdfBytes, (err) => {
      if (err) throw err;
    });

    return this.URL;
  };
}

// Handles the upload to AWS
function uploadPDF(pdfURL) {


  const location = Config.location;

  AWS.config.update({ region: location, signatureVersion: `v4` });

  AWS.config.loadFromPath("./config.json");

  const s3 = new AWS.S3();

  const accessPoint = Config.s3AccessPoint;
  const accountID = Config.awsAccountID;
  const region = Config.region;

  var uploadParams = {
    ACL: "public-read",
    Bucket: `arn:aws:s3:${region}:${accountID}:accesspoint/${accessPoint}`,
    Key: "",
    Body: null,
    ContentDisposition: "inline",
    ContentType: "application/pdf",
  };

  var file = "./generated/" + pdfURL;

  var fileStream = fs.createReadStream(file);
  fileStream.on("error", function (err) {
    console.log("File Error", err);
  });

  uploadParams.Body = fileStream;
  uploadParams.Key = path.basename(file);

  s3.upload(uploadParams, function (err, data) {
    if (err) {
      console.log("Error", err);
    }
    if (data) {
      // Deletes the localfile if the file was able to be uploaded to AWS
      fs.unlink(file, function (err) {
        if (err) throw err;
      });
    }
  });

  return `https://bloomreach-test.s3.${region}.amazonaws.com/${pdfURL}`;
}
